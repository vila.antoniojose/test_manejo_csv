from multiprocessing import get_context
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect, render
from django.urls import reverse, reverse_lazy

from django.views.generic import CreateView,DetailView

from Product.models import Product
from Product.forms import ProductForm

import csv
import math

# Create your views here.

def name_fileCsv(user_product,id_product):
    """ Funcion encargada de nombrar el archivo csv que se va a crear, recibe el usuario que creo el producto y el id del producto """
    return user_product + '_' + id_product +'.csv'

def WriteCsv(data):
    """ Funcion encargada de crear el producto recibe los una tupla que en su primera posición contiene el nombre del archivo que va a crear, 
    y en la segunda los datos del producto"""

    root = 'FilesCsv/'
    myFile = open(root+data[0],'w',encoding='utf-8')
    with myFile:
        writer = csv.writer(myFile)
        writer.writerows(data[1])

def read_filecsv(name_file):
    """ Función encargada de leer el archivo csv y retornar una tupla"""
    root = 'FilesCsv/'
    with open(root+name_file,newline='') as File:
        reader = csv.reader(File)
        product =[]
        for row in reader:
            if reader.line_num == 1:
                product =  row
        return product

def sales_projection(name_file):
    """ Función encargada de evaluar los datos del archivo csv para retornar la unidad por hora a vender """
    product = read_filecsv(name_file)
    if not product:
        return []
    else:
        product_name = product[1]
        product_estimated_time = int(product[-1])
        product_desired_amount_sales = int(product[-2])
        
        sales_by_hour = math.ceil(product_desired_amount_sales/product_estimated_time)
        
        return sales_by_hour


class ProductCreateView(CreateView):
    """ Vista encargada de crear los productos """
    model = Product
    template_name = "product/create.html"
    form_class = ProductForm
    success_url = reverse_lazy('homeView')

    def form_valid(self, form):
        self.object = form.save()
        
        product = self.object
    
        nameFile = name_fileCsv('antonio',str(product.id))
        dataProduct = [[product.id,product.name,product.category,product.description,product.desired_amount_sales,product.estimated_time]]
        mydata = [nameFile,dataProduct]

        WriteCsv(mydata)

        datos = sales_projection(nameFile)
        
        if not datos:
            return redirect(reverse_lazy('ErrorView'))
        else:
            return redirect(reverse_lazy('SalesProjectionDetailView',kwargs={'pk':product.id,'num_sell':datos}))

class SalesProjectionDetailView(DetailView):
    """ Vista encargada de mostra las proyecciones de venta o la unidad por hora a vender"""
    model = Product
    template_name = "Product/salesProjection.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["num_sell"] = self.kwargs['num_sell']
        return context
    