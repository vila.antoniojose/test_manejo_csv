from django import forms

from Product.models import Product

class ProductForm(forms.ModelForm):
    
    class Meta:
        model = Product
        fields = ("name","category","description","desired_amount_sales","estimated_time")
    