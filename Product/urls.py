from django.contrib import admin
from django.urls import path, include

from Product.views import ProductCreateView, SalesProjectionDetailView

urlpatterns = [
    path('create/', ProductCreateView.as_view(),name="ProductCreateView"),
    path('salesProjection/<int:pk>/<int:num_sell>/', SalesProjectionDetailView.as_view(),name="SalesProjectionDetailView"),
]
