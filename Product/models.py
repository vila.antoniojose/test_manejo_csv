from django.db import models

# Create your models here.

#category_choice = ((1,'food'),(2,'clothing')(3,'appliance'))

FOOD = 0
CLOTHING = 1
APPLIANCE = 2
CATEGORY_CHOICES = (
    (FOOD, 'Food'),
    (CLOTHING, 'Clothing'),
    (APPLIANCE, 'appliance'),
)

HOUR_DAY = 24
HOUR_WEEK = 168
HOUR_MONTH = 672
HOUR_YEAR = 8760


ESTIMATED_TIME_CHOICE = ((HOUR_DAY,'Día'),(HOUR_WEEK,'Semana'),(HOUR_MONTH,'Mes'),(HOUR_YEAR,'Año'),)

class Product(models.Model):
    name = models.CharField(verbose_name="Nombre",max_length=50)
    category = models.IntegerField(verbose_name="Categoria",choices=CATEGORY_CHOICES,null=True,blank=True)
    description = models.CharField(verbose_name="Descripción",max_length=250)
    desired_amount_sales = models.IntegerField(verbose_name="Proyección de ventas",default=1)
    estimated_time = models.IntegerField(verbose_name="Tiempo estimado",choices=ESTIMATED_TIME_CHOICE,null=True,blank=True)
    