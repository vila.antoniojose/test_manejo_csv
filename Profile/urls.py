from django.urls import include, path
from django.urls import path

from Profile.views import ErrorView, HomeView

urlpatterns = [
    path('', HomeView.as_view(),name="homeView"),
    path('error/', ErrorView.as_view(),name="ErrorView"),
] 
