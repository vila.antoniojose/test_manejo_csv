
# Test manejo de archivos csv y formularios web

En el proyecto decidí crear un Modelo producto para trabajar las operaciones que 
se solicitan en la prueba.

# ¿Cómo funciona la pagina?


Creé una interfaz que permite al usuario crear un nuevo producto para ser guardado 
en la base de datos.

Mediante un formulario se solicitan los datos pertinentes para guardar el registro
dentro de la base de datos y exportar dichos datos a un archivo csv.

El archivo se genera en una carpeta en la raiz de la pagina,este luego es leido
y analizado con el fin de usar sus datos para hacer una evaluación de la cantidad
de productos por hora que se deberían vender para cubrir una meta.

# ¿Como instalarlo?

- Se clona el repositorio en nuestro ordenador
- Se entra a la carpeta raiz
- Se crean las migraciones con el comando manage.py makemigrations
- Se implementan las migraciones con el comando manage.py migrate
- Se corre el servidor local con mange.py runserver